/**
 * http://usejsdoc.org/
 */

var connection = require("../mysqlConnection");
var cloudinary = require('cloudinary');
cloudinary.config({
	cloud_name: 'dtuygj8zu',
	api_key: '173378375398658',
	api_secret: '4R5IszpU3A5w7AQgs2SlniePQRk'
});
var async = require("async");
var posts = [];
var liked_post = [];

exports.index = function(req, res){
	if(typeof(req.session.login_user) == "undefined"){
		res.redirect('/login');
	}else{
		if(typeof(req.params.page) == "undefined"){
			res.status(404).send('そーいうことすんなよ。');
		}else{
			var async = require("async");
			posts =[];
			liked_post = [];
			var groups = [];
			var users = [];
			var query = 'select count(*) as count from contents;';
			var pages = 1;
			var showing_page = req.params.page;
			async.waterfall([
			    function(callback){
			    	connection.query(query, function(err, num_of_contents){
			    		while(num_of_contents[0].count > 6){
			    			num_of_contents[0].count -= 6;
			    			pages++;
			    		}
			    		if(pages == 1){
			    			query = "select * from contents order by created_date desc;"
			    		}else{
			    			var first = 6 * (showing_page - 1);
				    		query = 'select * from contents order by created_date desc limit 6 offset ' + first + ';';
			    		}
			    		callback(null, query);
			    	});
			    },
			    function(query, callback){
					connection.query(query, function(err, rows){
						if(err){
							res.send(err);
						}else{
							for(var i in rows){
								var post = {
									id: rows[i].id,
									user_id: rows[i].user_id,
									group_id: rows[i].group_id,
									title: rows[i].title,
									description: rows[i].description,
									media_id: rows[i].media_id,
									content_url: rows[i].content_url,
									user_name: "no name",
									thumbnail: "no image",
									likes: rows[i].likes
								};
								posts.push(post);
							}
							query = "select * from users;";
							callback(null, query);
						}
					});
			    },
			    function(query, callback){
			    	connection.query(query, function(err, user){
						if(err){
							console.log(err);
						}else{
							for(var j in user){
								users.push(user[j]);
							}
							query = "select * from groups;";
							callback(null, query);
						}
					});
			    },
			    function(query, callback){
			    	connection.query(query, function(err, group){
						if(err){
							console.log(err);
						}else{
							for(var k in group){
								groups.push(group[k]);
							}
							query = 'select * from likes where user_id = ' + req.session.login_user.id + ';';
							callback(null, query);
						}
					});
			    },
			    function(query, callback){
			    	connection.query(query,function(err, likes){
						if(err){
							console.log(err);
						}else{
							for(var i in likes){
								liked_post.push(likes[i].post_id);
							}
							var sample = "sample";
							callback(null, sample)
						}
					});
			    }
			],
			function(err, sample){
				if(err){
					console.log(err);
				}
				for(var i in posts){
					if(posts[i].user_id === null){
						for(var j in groups){
							if(posts[i].group_id == groups[j].id){
								posts[i].name = groups[j].group_name;
							}
						}
					}else{
						for(var j in users){
							if(posts[i].user_id == users[j].id){
								posts[i].name = users[j].user_name;
							}
						}
					}
				}
				for(var i in liked_post){
					console.log(liked_post[i]);
				}
				res.render('pexiv/index', {
					posts: posts,
					liked_post: liked_post,
					user_id: req.session.login_user.id,
					pages: pages,
					showing_page: showing_page
				});
			});
		}
	}
};

exports.like = function(req, res){
	if(typeof(req.session.login_user) == "undefined"){
		res.redirect('/login');
	}else{
		var post_id = req.body.post_id;
		var post = [];
		for(var i in posts){
			if(posts[i].id == post_id){
				post.push(posts[i]);
			}
		}
		var user_id = req.session.login_user.id;
		setTimeout(function(){
			var query = "select * from likes where user_id = " + user_id + " and post_id = " + post_id + ";";
			connection.query(query, function(err, rows){
				if(err){
					console.log(err);
				}else{
					console.log("rows.length:"+rows.length);
					if(rows.length == 0){
						query = "update contents set likes = likes + 1 where id = " + post_id + ";";
						connection.query(query, function(err, rows){
							if(err){
								console.log(err);
							}
						});
						query = "insert into likes(user_id, post_id)values(" + user_id + "," + post_id + ");";
						connection.query(query, function(err, rows){
							if(err){
								console.log(err);
							}
							console.log("プラス処理");
							res.send("{value: 0}");
						});
					}else{
						query = "update contents set likes = likes - 1 where id = " + post_id + ";";
						connection.query(query, function(err, rows){
							if(err){
								console.log(err);
							}
						});
						query = "delete from likes where user_id = " + user_id + " and post_id = " + post_id +";";
						connection.query(query, function(err, rows){
							if(err){
								console.log(err);
							}
							console.log("マイナス処理");
							res.send("{value: 0}");
						});
					}
				}
			});
		}, Math.floor(400 + Math.random() * 2000));
	}
};

exports.login = function(req, res){
	res.render('pexiv/login');
};


exports.loginpost = function(req, res){

	  var password = req.body.password;
	  var login_id = req.body.login_id;

	  var query = 'SELECT * FROM users WHERE login_id = "' + login_id + '" AND password = "' + password + '" LIMIT 1';
	  connection.query(query, function(err, rows) {
	    var user = rows.length? rows[0] : false;
	    if (user) {
	    	console.log(typeof(req.session));
	      req.session.login_user = user;
	      console.log("ログイン処理完了")
	      res.redirect('/1');
	    } else {
	      res.render('pexiv/login', {
	        title: 'ログイン',
	        noUser: 'ログインIDとパスワードが一致するユーザーはいません'
	      });
	    }
	  });
};

exports.signup = function(req, res){
	res.render('pexiv/signup');
};

exports.signuppost = function(req, res){
	var user_name = req.body.user_name;
	var login_id = req.body.login_id;
	var password = req.body.password;
	var age = req.body.age;
	var gender = req.body.gender;
	var number_of_hairs = req.body.number_of_hairs;

	var login_exists_query = 'SELECT * FROM users WHERE login_id = "' + login_id + '" LIMIT 1'; // 追加
	var signup_query = 'INSERT INTO users (user_name, login_id, password, age, gender, number_of_hairs, created_date, updated_date) VALUES ("' + user_name + '", ' + '"' + login_id + '", ' + '"' + password + '", ' +  age + ','  + gender + ',' + '"' +  number_of_hairs + '", CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);'; // 変更
	connection.query(login_exists_query, function(err, login) {
		var login_exists = login.length;
	    if (login_exists) {
	    	res.render('pexiv/signup', {
	    		user_name : user_name,
	    		login_id : login_id,
	    		age : age,
	    		gender : gender,
	    		self_introducion : self_introducion,
		        login_exists: '既に登録されているログインIDです'
		    });
	    } else {
	    	connection.query(signup_query, function(err, rows) {
	    		if(err){
	    			console.log(err);
	    		}
	    		res.redirect('/login');
	    	});
	    }
	});
};

exports.newpost = function(req, res){
	if(typeof(req.session.login_user) == "undefined"){
		res.redirect('/login');
	}else{
		var user_id = req.session.login_user.id;
		var query = "select * from groups where admin_user_id = " + user_id + ";";
		connection.query(query, function(err, rows){
			if(err){
				console.log(err);
			}else{
				res.render('pexiv/newpost',{
					login_user_id: user_id,
					my_groups: rows
				});
			}
		});
	}
};
exports.newpostpost = function(req, res){
	if(typeof(req.session.login_user) == "undefined"){
		res.redirect('/login');
	}else{
		var file_type = require('file-type');
		var read_chunk = require('read-chunk');
		var user_id = req.body.login_user_id;
		var title = req.body.title;
		var description = req.body.description;
		var path = req.file.path;
		var buffer = read_chunk.sync(path, 0, 10000);
		var media_id = 0;
		var who = req.body.who;
		if((file_type(buffer).ext != 'jpg') && (file_type(buffer).ext != 'png') && (file_type(buffer).ext != 'gif')){
			media_id = 1;
		}
		console.log(file_type(buffer).ext);
		cloudinary.uploader.upload(path, function(result){
			if(result.error){
				res.send(result.error);
			}else{
				var cloud_path = result.url;
				if(who == 'individual'){
					var query = 'insert into contents(user_id, title, description, media_id, content_url)values("'
						+user_id+'", "'+title+'", "'+description+'", "'+ media_id+'", "'+cloud_path+'");';
					connection.query(query, function(err, rows){
						if(err){
							res.send(err);
						}else{
							res.redirect('/1');
						}
					});
				}else{
					var group_id = req.body.group;
					var query = 'insert into contents(group_id, title, description, media_id, content_url)values("'
						+group_id+'", "'+title+'", "'+description+'", "'+ media_id+'", "'+cloud_path+'");';
					connection.query(query, function(err, rows){
						if(err){
							res.send(err);


						}else{
							res.redirect('/1');
						}
					});
				}
			}
		}, options={resource_type: 'raw'});
	}
};
exports.post = function(req, res){
	if(typeof(req.session.login_user)=="undefined"){
		res.redirect('/login');
	}else{
		var index = req.params.id;
		var post = [];
		for(var i in posts){
			if(posts[i].id == index){
				post.push(posts[i]);
			}
		}
		liked_post = [];
		var is_liked;
		var query = "select likes from contents where id = " + index + ";";
		connection.query(query, function(err, num_of_likes){
			console.log("num_of_likes:"+num_of_likes[0].likes);
			post[0].likes = num_of_likes[0].likes;
			query = 'select * from likes where user_id = ' + req.session.login_user.id + ' and post_id = ' + req.params.id + ';';
			connection.query(query,function(err, likes){
				if(err){
					console.log(err);
				}else if(likes.length == 1){
					is_liked = true;
				}else{
					is_liked = false;
				}
				res.render('pexiv/post', {
					post: post[0],
					is_liked: is_liked,
					id: req.session.login_user.id
				});
			});
		});
	}
};
exports.postedit = function(req, res){
	if(typeof(req.session.login_user) == "undefined"){
		res.redirect('/login');
	}else{
		var id = req.params.id;
		var post = [];
		for(var i in posts){
			if(posts[i].id == id){
				post.push(posts[i]);
			}
		}
		if(post[0].group_id === null){
			if(post[0].user_id != req.session.login_user.id){
				res.status(404).send('あれー、えー、足りないなー。黒いあれが足りないなー。髪の毛生やして出直して来な。');
			}else{
				res.render('pexiv/postedit', {post: post[0],
					id : req.session.login_user.id});
			}
		}else{
			var query = "select admin_user_id from groups where id = " + post[0].group_id + ";";
			connection.query(query, function(err, contents){
				if(err){
					console.log(err);
				}else{
					if(post[0].group_id != contents[0].admin_user_id){
						res.status(404).send('あれー、えー、足りないなー。黒いあれが足りないなー。髪の毛生やして出直して来な。');
					}else{
						res.render('pexiv/postedit', {post: post[0],
							id : req.session.login_user.id});
					}
				}
			});
		}

	}
};

exports.postdelete = function(req, res){
	if(typeof(req.session.login_user) == "undefined"){
		res.redirect('/login');
	}else{
		var post_id = req.params.id;
		var query = "delete from contents where id = " + post_id + ";";
		connection.query(query, function(err, rows){
			if(err){
				console.log(err);
			}
			res.redirect('/1');
		});
	}
}

exports.posteditput = function(req, res){
	if(typeof(req.session.login_user) == "undefined"){
		res.redirect('/login');
	}else{
		var post_id = req.params.id;
		var title = req.body.title;
		var description = req.body.description;
		var query = "update contents set title = '" + title + "',description = '" + description +"' where id = " + post_id + ";";
		connection.query(query, function(err, rows){
			if(err){
				console.log(err);
			}else{
				res.redirect('/1');
			}
		});
	}
};

exports.useredit = function(req, res){
	if(typeof(req.session.login_user)=="undefined"){
		res.redirect('/login');
	}else{
		if(req.params.id != req.session.login_user.id){
			res.status(404).send('あれー、えー、足りないなー。黒いあれが足りないなー。髪の毛生やして出直して来な。');
		}else{

			var user_id = req.session.login_user.id;
			var my_posts = [];
			for(var i in posts){
				if(posts[i].user_id == user_id){
					my_posts.push(posts[i]);
				}
			}
			res.render('pexiv/usersetting', {
				user: req.session.login_user,
				posts: my_posts
			});
		}
	}
};

exports.user = function(req, res){
	if(typeof(req.session.login_user)=="undefined"){
		res.redirect('/login');
	}else{
		var query = "select * from users where id = " + req.params.id + ";";
		var post_query = "select * from contents where user_id = " + req.params.id + ";";
		var posts = [];
		connection.query(query, function(err, user){
			if(err){
				res.send(err);
			}else if(user.length != 1){
				res.send('illegal state exception');
			}else{
				connection.query(post_query, function(err, post){
					if(err){
						res.send(err);
					}else{
						for(var i in post){
							posts.push(post[i]);
						}
						console.log(posts);
						res.render('pexiv/user',{
							user: user[0],
							posts: posts,
							login_user: req.session.login_user
						});
					}
				});
			}
		});
	}
};
exports.usereditput = function(req, res){
	if(typeof(req.session.login_user) == "undefined"){
		res.redirect('/login');
	}else{
		var id = req.params.id;
		var number_of_hairs = req.body.number_of_hairs;
		var name = req.body.user_name;
		var login_id = req.body.login_id;
		var password;
		if(req.body.password == ""){
			password = req.session.login_user.password;
		}else{
			password = req.body.password;
		}
		var self_introduction = req.body.self_introduction;
		var query = "update users set number_of_hairs = " + number_of_hairs + ", user_name = '" + name + "', self_introduction = '" + self_introduction + "', login_id = '" + login_id + "', password = '" + password + "' where id = " + id + ";";
		console.log(query);
		connection.query(query, function(err, rows){
			if(err){
				console.log(err);
			}else{
			req.session.login_user.number_of_hairs = req.body.number_of_hairs;
			req.session.login_user.login_id = req.body.login_id;
			req.session.login_user.password = password;
			req.session.login_user.user_name = name;
			res.redirect('/user/' + id);
			}
		});
	}
};

exports.grouplist = function(req, res){
	if(typeof(req.session.login_user) == "undefined"){
		res.redirect('/login');
	}else{
		var query = ("select * from groups where admin_user_id = " + req.session.login_user.id +
					" or user_id_1 = " + req.session.login_user.id +
					" or user_id_2 = " + req.session.login_user.id +
					" or user_id_3 = " + req.session.login_user.id +
					" or user_id_4 = " + req.session.login_user.id +
					" or user_id_5 = " + req.session.login_user.id +
					" or user_id_6 = " + req.session.login_user.id +
					" or user_id_7 = " + req.session.login_user.id +
					" or user_id_8 = " + req.session.login_user.id +
					" or user_id_9 = " + req.session.login_user.id );
		console.log(query);

		connection.query(query,function(err,groups){
			if(err){
				res.send(err);
			}else{
				res.render('pexiv/grouplist', {group_list : groups, id: req.session.login_user.id});		}
		});
	}
};

exports.group = function(req, res){
	if(typeof(req.session.login_user) == "undefined"){
		res.redirect('/login');
	}else{
		var async = require("async");
		var id = req.params.id;
		var group_query = "select groups.*, users.user_name from groups inner join users on groups.admin_user_id = users.id where groups.id = " + id;
		var posts = [];
		var get_group ;
		var get_users ;
		var j = 0;

		connection.query(group_query, function(err,group){
			if(err){
				console.log(err);
			}else {
				get_group = group;

				var user_list = [];
				if(get_group[0].user_id_1 !== null){
					user_list.push(get_group[0].user_id_1);
				}
				if(get_group[0].user_id_2 !== null){
					user_list.push(get_group[0].user_id_2);
				}
				if(get_group[0].user_id_3 !== null){
					user_list.push(get_group[0].user_id_3);
				}
				if(get_group[0].user_id_4 !== null){
					user_list.push(get_group[0].user_id_4);
				}
				if(get_group[0].user_id_5 !== null){
					user_list.push(get_group[0].user_id_5);
				}
				if(get_group[0].user_id_6 !== null){
					user_list.push(get_group[0].user_id_6);
				}
				if(get_group[0].user_id_7 !== null){
					user_list.push(get_group[0].user_id_7);
				}
				if(get_group[0].user_id_8 !== null){
					user_list.push(get_group[0].user_id_8);
				}
				if(get_group[0].user_id_9 !== null){
					user_list.push(get_group[0].user_id_9);
				}

		    	var query = "select * from contents where group_id = " + id + ";";
		    	connection.query(query, function(err,post){
		    		if(err){
						console.log(err);
					}else{
						for(var i in post){
							posts.push(post[i]);
						}
					}
		    	});

				var get_users = [];
				var key = user_list.length ;
				console.log("key"+key);
				for(var u = 0;u <= key; u++){
					async.waterfall([
					    function(callback){
							var user_query = "select * from users where id =" + user_list[u];

					    	callback(null,user_query);
					    },
					    function(user_query, callback){
							connection.query(user_query, function(err,user){
								if(err){
									console.log(err);
								}else {
									get_users.push(user[0]);
									callback(null, get_users);
								}
							});
					    }
						],
						function(err,get_users){
							j++;
							if(j == key){
								query = ("select * from groups where admin_user_id = " + req.session.login_user.id +
										" or user_id_1 = " + req.session.login_user.id +
										" or user_id_2 = " + req.session.login_user.id +
										" or user_id_3 = " + req.session.login_user.id +
										" or user_id_4 = " + req.session.login_user.id +
										" or user_id_5 = " + req.session.login_user.id +
										" or user_id_6 = " + req.session.login_user.id +
										" or user_id_7 = " + req.session.login_user.id +
										" or user_id_8 = " + req.session.login_user.id +
										" or user_id_9 = " + req.session.login_user.id );

								connection.query(query,function(err,groups){
									if(err){
										res.send(err);
									}else{
										res.render('pexiv/group', {group: get_group[0],
																   users : get_users,
																   posts : posts,
																   login_user: req.session.login_user,
																   groups: groups
																	});
									}
								});
							}
						}
					);
				}
			}
		});
	}
};
exports.groupsetting = function(req, res){
	if(typeof(req.session.login_user) == "undefined"){
		res.redirect('/login');
	}else{
		var id = req.params.id;
		var group_query = "select groups.*, users.user_name from groups inner join users on groups.admin_user_id = users.id where groups.id = " + id;
		var get_group ;
		var get_users ;
		var j = 0;

		connection.query(group_query, function(err,group){
			if(err){
				console.log(err);
			}else {
				if(group[0].admin_user_id != req.session.login_user.id){
					res.send("おっとっと!");
				}else{

					get_group = group;
					console.log(get_group[0].id);

					var user_list = [];
					if(get_group[0].user_id_1 !== null){
						user_list.push(get_group[0].user_id_1);
					}
					if(get_group[0].user_id_2 !== null){
						user_list.push(get_group[0].user_id_2);
					}
					if(get_group[0].user_id_3 !== null){
						user_list.push(get_group[0].user_id_3);
					}
					if(get_group[0].user_id_4 !== null){
						user_list.push(get_group[0].user_id_4);
					}
					if(get_group[0].user_id_5 !== null){
						user_list.push(get_group[0].user_id_5);
					}
					if(get_group[0].user_id_6 !== null){
						user_list.push(get_group[0].user_id_6);
					}
					if(get_group[0].user_id_7 !== null){
						user_list.push(get_group[0].user_id_7);
					}
					if(get_group[0].user_id_8 !== null){
						user_list.push(get_group[0].user_id_8);
					}
					if(get_group[0].user_id_9 !== null){
						user_list.push(get_group[0].user_id_9);
					}

					var get_users = [];
					var key = user_list.length ;
					for(var u = 0;u <= key; u++){
						async.waterfall([
						    function(callback){
								var user_query = "select * from users where id =" + user_list[u];
						    	callback(null,user_query);
						    },
						    function(user_query, callback){
								connection.query(user_query, function(err,user){
									if(err){
										console.log(err);
									}else {
										get_users.push(user[0]);
										callback(null, get_users);
									}
								});
						    }
							],
							function(err,get_users){
								j++;
								if(j == key){
									res.render('pexiv/groupsetting', {group: get_group[0],
																	   users : get_users,
																	   user: req.session.login_user

																		});
								}
							}
						);
					}
				}
			}
		});
	}
};
exports.groupsettingpost = function(req, res){
	var id = req.params.id;
	var self_introduction = req.body.self_introduction;
	var name = req.body.name;
	var query = "update pexiv.groups set group_name = '" + name + "', self_introduction = '" + self_introduction + "' where id =" + id + ";";
	connection.query(query,function(err,group){
		if(err){
			res.send(err);
		}else {
			res.redirect('/');
		}
	});

}
exports.groupregist = function(req, res) {
	if(typeof(req.session.login_user) == "undefined"){
		res.redirect('/login');
	}else{
		console.log(typeof(req.session));
		res.render('pexiv/groupregist', {
			  id: req.session.login_user.id,
			  group_id: req.params.id
		});
	}
};

exports.logout = function(req, res){

	  req.session.destroy();
	  res.redirect('/login');
};