

/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , pexiv = require('./routes/pexiv')
  , http = require('http')
  , path = require('path')
  , multer = require("multer")
  , socket = require("socket.io")
  , connection = require("./mysqlConnection")
  , moment = require("moment")
  , upload = multer({dest: './public/images/'});


var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.urlencoded());
app.use(express.json());
app.use(express.cookieParser());
app.use(express.session({
	secret: 'keyboard cat',
	resave: true,
	saveUnintialized: true,
	cookie: {
		maxAge: 1000000
	}
}));
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/:page([0-9]+)', pexiv.index);
app.get('/login', pexiv.login);
app.post('/login', pexiv.loginpost);
app.get('/signup', pexiv.signup);
app.post('/signup', pexiv.signuppost);
app.get('/newpost', pexiv.newpost);
app.post('/newpost', upload.single('file'), pexiv.newpostpost);
app.get('/post/:id([0-9]+)', pexiv.post);
app.get('/post/:id([0-9]+)/edit', pexiv.postedit);
app.put('/post/:id([0-9]+)/edit', pexiv.posteditput);

app.get('/groupsetting/:id([0-9]+)', pexiv.groupsetting);
app.post('/groupsetting/:id([0-9]+)', pexiv.groupsettingpost);
app.get('/group/:id([0-9]+)', pexiv.group);
app.get('/grouplist', pexiv.grouplist);
app.get('/groupregist', pexiv.groupregist);
app.get('/user/:id([0-9]+)', pexiv.user);
app.get('/user/:id([0-9]+)/edit', pexiv.useredit);
app.put('/user/:id([0-9]+)/edit', pexiv.usereditput);
app.post('/ajax', pexiv.like);
app.get('/logout', pexiv.logout);
app.delete('/post/:id([0-9]+)', pexiv.postdelete);



app.get('/', pexiv.index);
app.get('/', pexiv.index);
app.get('/', pexiv.index);
app.get('/users', user.list);





var server = http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var io = socket.listen(server);

io.sockets.on('connection', function(socket){
	var name;
	var room;
	socket.on('join_room', function(data){
		var room = data.keyword;
		socket.join(room);
	});
	socket.on('first_client_to_server', function(data){
		room = data.keyword;
		var id = socket.id;


		var sql = 'select count(*) as count from individuals_chats where keyword = "' + room + '";';
		connection.query(sql, function(err, rows){
			if(err){
				console.log(err);
			}else{
				var start_line = rows[0].count -7;
				if(start_line<1){
					start_line = 1;
				}
				sql = 'select * from ' + data.table_name + ' where keyword = "'+ room + '" order by created_date asc limit 7 offset ' + start_line + ";";
				connection.query(sql, function(err, rows){
					if(err){
						console.log(err);
					}else{
						for(var i in rows){
							rows[i].created_date = Calculate_Time(rows[i].created_date);
						}
						io.to(id).emit('first_server_to_client', {
							"all_messages": rows
						});
					}
				});
			}
		});
	});
	socket.on('first_client_to_server_group', function(data){
		room = data.keyword;
		var id = socket.id;
		var async = require("async");
		var messages = [];
		async.waterfall([
		     function(callback){
		    	 var sql = 'select count(*) as count from ' + data.table_name + ' where group_id = "' + room + '";';
		 		connection.query(sql, function(err, rows){
		 			if(err){
		 				console.log(err);
		 			}else{
		 				var start_line = rows[0].count -7;
		 				if(start_line<1){
		 					start_line = 1;
		 				}
		 				sql = 'select * from ' + data.table_name + ' where group_id = "'+ room + '" order by created_date asc limit 7 offset ' + start_line + ";";
		 				console.log(sql);
		 				callback(null, sql);
		 			}
		 		});
		     },
		     function(sql, callback){
		    	 connection.query(sql, function(err, message){
		 			if(err){
		 				console.log(err);
		 			}else{
		 				for(var i in message){
			 				var new_message = {
			 					sender_id: message[i].user_id,
			 					group_id: message[i].group_id,
			 					text: message[i].text,
			 					sender_name: "no name",
			 					created_date: Calculate_Time(message[i].created_date)
			 				};
			 				messages.push(new_message);
		 				}
		 				callback(null, messages);
		 			}
		 		});
		     },
		     function(messages, callback){
		    	 sql = 'select * from users;';
		    	 connection.query(sql, function(err, users){
		    		 if(err){
		    			 console.log(err);
		    		 }else{
		    			 for(var i in messages){
		    				 for(var j in users){
		    					 if(messages[i].sender_id == users[j].id){
		    						 messages[i].sender_name = users[j].user_name;
		    					 }
		    				 }
		    			 }
		    			 callback(null, messages);
		    		 }
		    	 });

		     }
		],
		function(err, messages){
			io.to(id).emit('first_server_to_client_group', {
				"all_messages": messages
			});
		});
	});
	socket.on('client_to_server', function(data){
		room = data.keyword;
		socket.join(room);
		var query = 'insert into ' + data.table_name + ' (receiver_id, sender_id, text, keyword)values(' + data.receiver_id + ', ' + data.sender_id + ', "' + data.text + '", "' + data.keyword + '");';
		connection.query(query, function(err, rows){
			if(err){
				console.log(err);
			}else{
				console.log(data.text + "," + data.sender_id);
				io.to(room).emit('server_to_client', {
					"message": data.text,
					"sender_id": data.sender_id
				});
			}
		});
	});

	socket.on('client_to_server_group', function(data){
		room = data.keyword;
		socket.join(room);
		var query = 'insert into ' + data.table_name + ' (user_id, group_id, text)values(' + data.sender_id + ', "' + data.keyword + '", "' + data.text + '");';
		connection.query(query, function(err, rows){
			if(err){
				console.log(err);
			}else{
				console.log(data.text + "," + data.sender_id);
				io.to(room).emit('server_to_client_group', {
					"message": data.text,
					"sender_id": data.sender_id,
					"sender_name": data.sender_name
				});
			}
		});
	});

	socket.on("look_up_user", function (user_id) {
		console.log("look_up_user");
//		console.log(user_id);
		var login_id = user_id;
		var get_user_query =  'SELECT * FROM users WHERE login_id = "' + login_id + '" LIMIT 1';
//		console.log(get_user_query);
		connection.query (get_user_query,function(err, user){
			var user_exists = user.length;
			if(user_exists){
				console.log(user[0]);
				socket.emit("geting_user" ,user[0]);
			}else{
				socket.emit("nothing_user" ,'ユーザーが見つかりません');
			}
		});
	});

	socket.on("group_regist", function(group_data){
		var admin_user_id = group_data.login_id;
		var group_name = group_data.group_name;
		var group_users = group_data.group_users;
		console.log(admin_user_id[0]);
		console.log(group_name[0]);
		console.log(group_users["tanaka1"]);

		var add_user_query = 'INSERT INTO groups (group_name, admin_user_id';
		var add_user_size = "";
		var group_size = Object.keys(group_users);
		  for(var i in group_size){
			add_user_size = add_user_size + ', user_id_' + (parseInt(i,10)+1);
		}
		console.log(add_user_size);

		add_user_query = add_user_query + add_user_size;
		add_user_query = add_user_query + ', created_date, updated_date ) VALUES ("' + group_name + '", ' + admin_user_id ;
		var add_user_id = "";

		  for(var key in group_users){
			add_user_id = add_user_id + ', ' +  group_users[key].id;
		}
		console.log(add_user_id);
		add_user_query = add_user_query + add_user_id;
		add_user_query = add_user_query + ', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);'; // 変更

		console.log(add_user_query);

		connection.beginTransaction(function(err) {
			  if (err) { throw err; }
			  connection.query(add_user_query, function(err, group) {
			    if (err) {
			      //insertに失敗したら戻す
			      connection.rollback(function() {
			    	  console.log("登録失敗");
//			        throw err;
				    var err_mes = '登録に失敗しました';
				    socket.emit('regist_err', err_mes);
			      });
			    }else{
			    	connection.commit(function(err) {
			    		if (err) {
			    			connection.rollback(function() {
			    		  		throw err;
			    			});
			    		}
			    	});
				    console.log("登録成功");
				    console.log(group);
				    var destination = './grouplist';
				    socket.emit('redirect', destination);
			    };

			  });
			  });
	});
});

function Calculate_Time(created_date){

	var createdDate = Date.parse(created_date);
	var now = moment().toDate()
	var diffInSeconds = (now.getTime() - createdDate)/1000;

	var numOfWeeks = 0;
	var numOfDays = 0;
	var numOfHours = 0;
	var numOfMinutes = 0;
	var numOfSeconds = 0;
	var timeMessage;
	if(diffInSeconds > 1209600){
		$(this).siblings(".commentDate").text($(this.val()));
		return;
	}
	while(diffInSeconds >= 86400){
		numOfDays++;
		diffInSeconds -= 86400;
	}
	while(diffInSeconds >= 3600){
		numOfHours++;
		diffInSeconds -= 3600;
	}
	while(diffInSeconds >= 60){
		numOfMinutes++;
		diffInSeconds -= 60;
	}
	numOfSeconds = Math.round(diffInSeconds);

	if(numOfWeeks != 0){
		timeMessage = numOfWeeks+"週間"
	}else if(numOfDays != 0){
		timeMessage = numOfDays+"日";
	}else if(numOfHours != 0){
		timeMessage = numOfHours+"時間";
	}else if( numOfMinutes != 0){
		timeMessage = numOfMinutes+"分";
	}else{
		timeMessage = numOfSeconds+"秒";
	}
	timeMessage = timeMessage + "前";
	return timeMessage;
};


