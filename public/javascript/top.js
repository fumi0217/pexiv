$("figure").mouseleave(
  function() {
    $(this).removeClass("hover");
  }
);
$(".btn").mouseover(function(){
 var name = $(this).attr("id");
	$(this).text(name);

	});




$(".btn").mouseover(function(){

	$(this).text("ユーザーページへ");

});



  function() {


});



(function() {

  var initialise = function() {
    $(".js-likeButton").on("click", onClickLikeButton);
  };

  var onClickLikeButton = function() {
    var $thisButton = $(this);
    var button = {
      $likeButton: $thisButton,
      $likeIcon: $thisButton.find(".js-likeIcon"),
      $loadingIcon: $thisButton.find(".js-likeLoadingIcon"),
      userHasLikedThisPost: $thisButton.hasClass("is-liked")
    };
    likeOrUnlikeSequence(button);
  };

  var likeOrUnlikeSequence = function(button) {
    var sendLikeRequest = likeRequest(button);
    button.$likeButton.prop("disabled", true);
    button.$likeButton.addClass("is-loading");
    return sendLikeRequest
      .then(postLikeSucceeded)
      .catch(postLikeFailed);
  }

  var likeRequest = function(button) {
    return new Promise(function(resolve, reject) {
      fake_ajax_response()
        .done(function likePostAjaxDone() {
          resolve(button);
        })
        .fail(function likePostAjaxFail() {
          reject(button)
        });
    });
  };

  var postLikeSucceeded = function(responses) {
    var button = responses;
    console.log(button);
    var $likesDisplay = button.$likeButton.siblings(".js-numberOfLikes")
    var numberOfLikes = $likesDisplay.text();

    if (button.userHasLikedThisPost) {
      numberOfLikes--;
      button.$likeButton.removeClass("is-liked");
    } else {
      numberOfLikes++;
      button.$likeButton.addClass("is-liked");
    }
    $likesDisplay.text(numberOfLikes);
    button.$likeButton.prop("disabled", false);
    button.$likeButton.removeClass("is-loading");
  };

  var postLikeFailed = function(button) {
    console.log("Like Failed");
    button.$likeButton.prop("disabled", false);
    button.$likeButton.removeClass("is-loading");
  };



  // TODO: remove this function when ajax call added
  function fake_ajax_response(response) {
    var deferred = $.Deferred();
    setTimeout(function() {
      deferred.resolve(response);
    }, Math.floor(400 + Math.random() * 2000));
    return deferred.promise();
  }

  initialise();

})();


$('.btn_menu').click(function(){
    $(this).toggleClass('active');
    $('nav').toggleClass('open');
    if($("#fadeLayer").css("visibility") == "hidden") {
    	$("#fadeLayer").css("visibility", "visible");
    } else {
    	$("#fadeLayer").css("visibility", "hidden");
    }
  });

$('#fadeLayer').click(function(){
	$('.btn_menu').removeClass('active');
	$('nav').removeClass('open');
	var target = document.getElementById("fadeLayer");
    target.style.visibility = "hidden";
});
