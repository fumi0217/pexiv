$("button").click(function () {
  var target = $(this);
  if (target.hasClass("done")) {
    // Do nothing
  } else {
    target.addClass("processing");
    setTimeout(function () {
      target.removeClass("processing");
      target.addClass("done");
    }, 100000000000000000);
  }
});

$("#group").click(function(){
	  if($(".group").css("display") == "none"){
	    $(".non-group").hide(200);

	    $(".group").show(50);
	    $("#group").text("個人投稿");

	  }else{
	    $(".group").hide(150);
	    $(".non-group").show(200);
	    $("#group").text("グループ投稿");
	  }
	});


$('.btn_menu').click(function(){
    $(this).toggleClass('active');
    $('nav').toggleClass('open');
    if($("#fadeLayer").css("visibility") == "hidden") {
    	$("#fadeLayer").css("visibility", "visible");
    } else {
    	$("#fadeLayer").css("visibility", "hidden");
    }
  });

$('#fadeLayer').click(function(){
	$('.btn_menu').removeClass('active');
	$('nav').removeClass('open');
	var target = document.getElementById("fadeLayer");
    target.style.visibility = "hidden";
});



