$('.form').find('input, textarea').on('keyup blur focus', function (e) {

  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight');
			} else {
		    label.removeClass('highlight');
			}
    } else if (e.type === 'focus') {

      if( $this.val() === '' ) {
    		label.removeClass('highlight');
			}
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});





$('.form').find('select').change(function (e) {

	  var $this = $(this),
	      label = $this.prev('label');

	  if( $this.val() === "0" ) {
		  label.removeClass('active highlight');
			}
    else if( $this.val() !== "0" ) {
		    label.addClass('active highlight');
			}
	});

$('.tab a').on('click', function (e) {

  e.preventDefault();

  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');

  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();

  $(target).fadeIn(600);

});


$(".intro").click(function(){
	$(this).hide("200");
	$(".wrapper").fadeIn("200");
	$(".p_rogo").fadeIn("400");
});

$(function(){
    setTimeout(function(){
        $(".intro").fadeOut("slow");
        $(".p_rogo").fadeIn("slow");
    },3500);
    $(".wrapper").show("200");
});
