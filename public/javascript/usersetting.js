$("#contents_show").click(function(){
  if($("#contents").css("display") == "none"){
    $(".info").hide(200);

    $("#contents").show(50);
    $("#contents_show").text("ユーザー情報を表示する");

  }else{
    $("#contents").hide(150);
    $(".info").show(200);
    $("#contents_show").text("投稿作品一覧を表示する");
  }
});

$('.btn_menu').click(function(){
    $(this).toggleClass('active');
    $('nav').toggleClass('open');
    if($("#fadeLayer").css("visibility") == "hidden") {
    	$("#fadeLayer").css("visibility", "visible");
    } else {
    	$("#fadeLayer").css("visibility", "hidden");
    }
  });

$('#fadeLayer').click(function(){
	$('.btn_menu').removeClass('active');
	$('nav').removeClass('open');
	var target = document.getElementById("fadeLayer");
    target.style.visibility = "hidden";
});

